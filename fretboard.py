#!/usr/bin/python3

import bisect
import random
# import winsound

import cv2
import numpy as np

from mypackage.imshow_resize import imshow_540


class GuitarString:
    def __init__(self, base_frequency, xy_neck, xy_bridge):
        self.frequency = np.array([2 ** (f / 12) for f in range(0, 25)]) * base_frequency
        (self.x_neck, self.y_neck) = xy_neck
        (self.x_bridge, self.y_bridge) = xy_bridge
        self.fret_loc_norm = np.array([(1 - 1 / (2 ** (f / 12))) for f in range(0, 25)])
        self.fret_x = self.x_neck + self.fret_loc_norm * (self.x_bridge - self.x_neck)
        self.fret_y = self.y_neck + self.fret_loc_norm * (self.y_bridge - self.y_neck)
        print(self.frequency)

    def fingerxy(self, fret):
        x_f = int(0.7 * self.fret_x[fret] + 0.3 * self.fret_x[fret - 1])
        y_f = int(0.7 * self.fret_y[fret] + 0.3 * self.fret_y[fret - 1])
        return (x_f, y_f)

    def playnote(self, fret, duration):
        #winsound.Beep(int(self.frequency[fret]), duration)
        pass


COLOR_BGR_NUT = (255, 0, 0)
COLOR_BGR_FRET = (128, 196, 222)
COLOR_BGR_BOARD = (32, 64, 96)
COLOR_BGR_INLAY = (255, 220, 200)
COLOR_BGR_FINGER = (26, 32, 255)
finger_radius = 8

fret_loc_norm = np.array([(1 - 1 / (2 ** (f / 12))) for f in range(0, 25)])  # fret number as index
notes = ['A', 'Bb', 'B', 'C', 'C#', 'D', 'Eb', 'E', 'F', 'F#', 'G', 'Ab']
tuning_index = {"standard": [7, 0, 5, 10, 2, 7]}

padding_x = 40
padding_y = 20
neck_width = 102
x0 = padding_x
x7 = padding_x + neck_width
string_x = []
neck_y = padding_y
string_length = 920
bridge_y = padding_y + string_length
fret_y = np.array((neck_y + string_length * fret_loc_norm), dtype=int)

freq_open = [82, 110, 147, 196, 247, 330]  # (string number - 1) as index
str_x = [padding_x + round(neck_width * ((s + 0.5) / 6)) for s in range(0, 6)]
gt_str = [GuitarString(freq_open[s], (str_x[s], neck_y), (str_x[s], bridge_y)) for s in range(0, 6)]

img = np.zeros((string_length + 2 * padding_y, neck_width + 2 * padding_x, 3), dtype=np.uint8)
cv2.rectangle(img, (x0, neck_y), (x7, fret_y[24]), COLOR_BGR_BOARD, thickness=-1)

cv2.line(img, (x0, neck_y), (x7, neck_y), COLOR_BGR_NUT, thickness=4)
cv2.line(img, (x0, bridge_y), (x7, bridge_y), COLOR_BGR_NUT, thickness=4)

for fret in range(1, 24):
    y = int(gt_str[0].fret_y[fret])
    cv2.line(img, (x0, y), (x7, y), COLOR_BGR_FRET, thickness=2)

string_width = [3, 3, 2, 1, 1, 1]
for s in range(0, 6):
    x = padding_x + round(neck_width * ((s + 0.5) / 6))
    cv2.line(img, (x, neck_y), (x, bridge_y), (222, 222, 222), thickness=string_width[s])

for fret in [3, 5, 7, 9, 12, 15, 17, 19, 21]:
    y = int((fret_y[fret] + fret_y[fret - 1]) / 2)
    if fret != 12:
        xx = [int(padding_x + neck_width / 2)]
    else:
        xx = [int(padding_x + neck_width * 2 / 6), int(padding_x + neck_width * 4 / 6)]
    for x in xx:
        cv2.circle(img, (x, y), 4, COLOR_BGR_INLAY, thickness=-1)

# cv2.imshow('Fretboard', data)

img_guitar_ori = cv2.imread('data/img_2515_guitar.jpg')
xy_corners_photo = np.array([[1107, 128 - 1], [1113, 198 + 1], [765, 383], [750, 285]], dtype="float32")
warp_wid = xy_corners_photo[1, 0] - xy_corners_photo[3, 0]
warp_hig = xy_corners_photo[2, 1] - xy_corners_photo[0, 1]

for (xx, yy) in xy_corners_photo:
    cv2.circle(img_guitar_ori, (int(xx), int(yy)), finger_radius, COLOR_BGR_FRET, thickness=-1)

print(xy_corners_photo)
x1 = padding_x + round(neck_width * ((0 + 0.5) / 6))
x2 = padding_x + round(neck_width * ((5 + 0.5) / 6))
xy_corners_sch = np.array([[x1 + 10, neck_y], [x2 - 10, neck_y], [x2, fret_y[12]], [x1, fret_y[12]]], dtype="float32")
print(xy_corners_sch)
M = cv2.getPerspectiveTransform(xy_corners_sch, xy_corners_photo)
# print(M)

melody = [[5, 8], [3, 9], [3, 10], [5, 8]]

for ss in range(0, 6):
    for ff in range(5, 9):
        img_guitar = img_guitar_ori.copy()
        img_fingered = img.copy()
        (fingerx, fingery) = gt_str[ss].fingerxy(ff)
        cv2.circle(img_fingered, (fingerx, fingery), finger_radius, COLOR_BGR_FINGER, thickness=-1)
        xy_photo = cv2.perspectiveTransform(np.array([[(fingerx, fingery)]], dtype="float32"), M)
        (x1, y1) = xy_photo[0][0]
        cv2.circle(img_guitar, (int(x1), int(y1)), finger_radius, COLOR_BGR_FINGER, thickness=-1)
        imshow_540('Fretboard', img_fingered)
        imshow_540('Realworld View', img_guitar)
        gt_str[ss].playnote(ff, 650)
        cv2.waitKey(500)

random.seed()
while False:
    img_guitar = img_guitar_ori.copy()
    img_fingered = img.copy()
    fingerx = padding_x + round(random.random() * neck_width * 0.96)
    fingery = padding_y + round(random.random() * 0.73 * string_length)
    xy_finger = (fingerx, fingery)
    cv2.circle(img_fingered, xy_finger, finger_radius, COLOR_BGR_FINGER, thickness=-1)
    xy_photo = cv2.perspectiveTransform(np.array([[xy_finger]], dtype="float32"), M)
    (x1, y1) = xy_photo[0][0]
    cv2.circle(img_guitar, (int(x1), int(y1)), finger_radius * 2, COLOR_BGR_FINGER, thickness=-1)
    finger_string = int((fingerx - padding_x) / neck_width * 6)
    finger_fret = bisect.bisect_left(fret_y, fingery)
    freq_note = gt_str[finger_string].frequency[finger_fret]
    # semitone_from_A = round((math.log2(freq_note / 440) * 12)) % 12
    finger_note = notes[(tuning_index["standard"][finger_string] + finger_fret) % 12]
    print("String:{}, Fret:{}, Frequency: {:.2f} Hz, Note: {}".format(6 - finger_string, finger_fret, freq_note,
                                                                      finger_note))
    imshow_540('Fretboard', img_fingered)
    imshow_540('Realworld View', img_guitar)
    keyinput = cv2.waitKey(0) & 0xFF
#    if keyinput == 27:
#        break

cv2.destroyAllWindows()
