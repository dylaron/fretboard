
import cv2
import numpy as np

from mypackage.imshow_resize import imshow_540


class GuitarString:
    def __init__(self):
        self.open_frequency = 440
        self.x_n = 0
        self.y_n = 0
        self.x_b = 0
        self.y_b = 0


class GuitarFret:
    def __init__(self, fret_no):
        self.fret_no = fret_no
        self.x = 0
        self.y = 0

class GuitarPhysical:
    nut_string_span_ratio = 0.83
    bridge_string_span_ratio = 2 - nut_string_span_ratio
    fretboard_margin = .125  # inch each side

    def __init__(self, fret12_string_span, string_length):
        self.fret12_string_span = fret12_string_span
        self.string_length = string_length
        self.y_nut = string_length / 2
        self.y_bridge = - string_length / 2
        self.fret_12_x = - self.fret12_string_span / 2 - GuitarPhysical.fretboard_margin
        self.frets = [GuitarFret(i) for i in range(0, 25)]
        self.strings = [GuitarString() for i in range(0, 6)]
        for i in range(0, 6):
            self.strings[i].x_n = self.fret12_string_span * GuitarPhysical.nut_string_span_ratio * (i - 2.5) / 5
            self.strings[i].y_n = self.y_nut
            self.strings[i].x_b = self.fret12_string_span * GuitarPhysical.bridge_string_span_ratio * (i - 2.5) / 5
            self.strings[i].y_b = self.y_bridge
        self.nut_x_l = self.strings[0].x_n - GuitarPhysical.fretboard_margin
        self.bridge_x_l = self.strings[0].x_b - GuitarPhysical.fretboard_margin
        for i in range(0, 25):
            self.frets[i].y = self.string_length * (1 / (2 ** (i / 12)) - 0.5)
            self.frets[i].x = self.fret_12_x + (self.nut_x_l - self.fret_12_x) * self.frets[i].y / self.y_nut

    def tuning(self, open_frequency):
        for i in range(0, 6):
            self.strings[i].open_frequency = open_frequency[i]


class GuitarImage:
    ppi = 60  # pixels per inch
    padding = 20  # pixels

    def __init__(self, guitar_physical):
        self.guitar_physical = guitar_physical
        self.width = round(self.guitar_physical.fret12_string_span * GuitarImage.ppi + GuitarImage.padding * 2)
        self.height = round(self.guitar_physical.string_length * GuitarImage.ppi + GuitarImage.padding * 2)
        self.m_offset = (self.width / 2, self.height / 2)

    def render(self):
        img = np.zeros((self.height, self.width, 3), dtype=np.uint8)
        img = self.draw_frets(img)
        img = self.draw_strings(img)
        img = self.draw_nuts_bridge(img)
        return img

    def inch_pixel(self, xy_in):
        (x_in, y_in) = xy_in
        x_px = int(x_in * GuitarImage.ppi + self.m_offset[0])
        y_px = int(-y_in * GuitarImage.ppi + self.m_offset[1])
        return (x_px, y_px)

    def draw_frets(self, img):
        for fret in self.guitar_physical.frets:
            (x0, y0) = self.inch_pixel((-fret.x, fret.y))
            (x1, y1) = self.inch_pixel((fret.x, fret.y))
            cv2.line(img, (x0, y0), (x1, y1), (0, 192, 192), thickness=2)
        return img

    def draw_strings(self, img):
        for gtstr in self.guitar_physical.strings:
            (x0, y0) = self.inch_pixel((gtstr.x_n, gtstr.y_n))
            (x1, y1) = self.inch_pixel((gtstr.x_b, gtstr.y_b))
            cv2.line(img, (x0, y0), (x1, y1), (0, 192, 0), thickness=1)
        return img

    def draw_nuts_bridge(self, img):
        (x0, y0) = self.inch_pixel((self.guitar_physical.nut_x_l, self.guitar_physical.y_nut))
        (x1, y1) = self.inch_pixel((- self.guitar_physical.nut_x_l, self.guitar_physical.y_nut))
        cv2.line(img, (x0, y0), (x1, y1), (46, 128, 192), thickness=2)
        (x0, y0) = self.inch_pixel((self.guitar_physical.bridge_x_l, self.guitar_physical.y_bridge))
        (x1, y1) = self.inch_pixel((- self.guitar_physical.bridge_x_l, self.guitar_physical.y_bridge))
        cv2.line(img, (x0, y0), (x1, y1), (46, 128, 192), thickness=2)
        return img


apx500 = GuitarPhysical(42 / 25.4, 25)
img_apx500 = GuitarImage(apx500)
apx500.tuning([82, 110, 147, 196, 247, 330])

img = img_apx500.render()
imshow_540('fretboard', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# d.append(draw.Line(apx500.nut_x_l, apx500.y_nut, -apx500.nut_x_l, apx500.y_nut, stroke_width=0.07, stroke='black'))
# d.append(draw.Line(apx500.bridge_x_l, apx500.y_bridge, -apx500.bridge_x_l, apx500.y_bridge, stroke_width=0.07,
#                    stroke='black'))
