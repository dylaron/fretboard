#!/usr/bin/env python

import cv2

def imshow_540(title, imgisvga, out_texts=[]):
    (disp_w, disp_h) = (960, 540)
    aspect_ratio = imgisvga.shape[1] / imgisvga.shape[0]  # Width/Height
    if aspect_ratio > disp_w / disp_h:
        disp_h = int(disp_w / aspect_ratio)  # too wide
    else:
        disp_w = int(disp_h * aspect_ratio)  # too tall
    resized = cv2.resize(imgisvga, (disp_w, disp_h), interpolation=cv2.INTER_AREA)
    text_color = (0, 255, 255)
    for counter, out_text in enumerate(out_texts):
        cv2.putText(resized, out_text, (20, 40 + counter * 40),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.8, text_color, lineType=cv2.LINE_AA)
    cv2.imshow(title, resized)